import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { CrossRoadComponent } from './crossroad/crossroad.component ';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, CrossRoadComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'road_regulation_app';
}
