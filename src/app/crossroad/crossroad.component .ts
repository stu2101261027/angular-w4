import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from "@angular/core";
import {TrafficLightComponent} from "../traffic-light/traffic-light.component";
import { TrafficLightHorizontalComponent } from "app/traffic-light-horizontal/traffic-light-horizontal.component";

@Component({
  selector    : 'app-road',
  templateUrl : './crossroad.component.html',
  styleUrl    : './crossroad.component.css',
  standalone  : true,
  imports:[TrafficLightComponent, TrafficLightHorizontalComponent]
})
export class CrossRoadComponent {

  
  
}