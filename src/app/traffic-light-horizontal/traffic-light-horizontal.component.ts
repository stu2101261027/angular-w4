import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from "@angular/core";
import { CommonModule } from '@angular/common';

@Component({
  selector    : 'app-traffic-light-horizontal',
  templateUrl : './traffic-light-horizontal.component.html',
  styleUrl    : './traffic-light-horizontal.component.css',
  standalone  : true,
  imports     : [CommonModule]
})
export class TrafficLightHorizontalComponent implements OnInit{
  
  public trafficStatus='';
  @Output() public onStatusChange = new EventEmitter();
  public lights = {
    isRed: false,
    isYellow: false,
    isGreen: false
  };
  constructor() { }
  
  
  ngOnInit() {
    this.changeLights();
  }

  public changeLights() {
    this.changeRedLight();

    this.changeGreenLight();
    this.changeYellowLight();
    setInterval(()=> {
        this.changeRedLight();
        this.changeYellowLight();
        this.changeGreenLight();
    }, 5000);
}

public changeGreenLight() {
    setTimeout(()=>
      this.lights.isGreen=true, 2000);
    this.lights.isGreen=false;
}
public changeYellowLight() {
    setTimeout(()=> {
      this.lights.isGreen=true
        this.lights.isYellow=true;
    }, 3000);
    this.lights.isGreen=false;
    this.lights.isYellow=false;
}

public changeRedLight() {
    setTimeout(()=> {
     // this.lights.isYellow=true;
       this.lights.isRed=true;
    }, 0);
   // this.lights.isYellow=false;
    this.lights.isRed=false; 
}
}