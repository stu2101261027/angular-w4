import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from "@angular/core";
import { CommonModule } from '@angular/common';

@Component({
  selector    : 'app-traffic-light',
  templateUrl : './traffic-light.component.html',
  styleUrl    : './traffic-light.component.css',
  standalone  : true,
  imports     : [CommonModule]
})
export class TrafficLightComponent implements OnInit{
  
  public trafficStatus='';
  @Output() public onStatusChange = new EventEmitter();
  public lights = {
    isRed: false,
    isYellow: false,
    isGreen: false
  };
  constructor() { }
  
  public setLightStatus(){

    if(this.lights.isRed=true){
      this.lights.isYellow=true;
      
    }
  }
  
  ngOnInit() {
    this.changeLights();
  }

  public changeLights() {
    
    this.changeGreenLight();
    this.changeYellowLight();
    this.changeRedLight();

    setInterval(()=> {
        this.changeGreenLight();
        this.changeYellowLight();
        this.changeRedLight();
    }, 5000);
}


public changeGreenLight() {
    setTimeout(()=>
      this.lights.isGreen=true, 0);
    this.lights.isGreen=false;
}
public changeYellowLight() {
    setTimeout(()=> {
      //this.lights.isGreen=true
        this.lights.isYellow=true;
    }, 2000);
    //this.lights.isGreen=false;
    this.lights.isYellow=false;
}

public changeRedLight() {
    setTimeout(()=> {
     // this.lights.isYellow=true;
       this.lights.isRed=true;
    }, 3000);
   // this.lights.isYellow=false;
    this.lights.isRed=false;

    
}
public allLightsYellow(){
  setInterval(()=> {
    // this.lights.isYellow=true;
      this.lights.isRed=false;
      
   }, 10000);

}
  
}